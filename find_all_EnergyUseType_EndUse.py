#!/usr/bin/python3
'''
I'm trying to fins out what all the EnergyUseTypes are and all the EndUses are. Eplus seems to take
a pretty relaxed approach to defining a huge and diverse ontology - poor docs too ... I guess there
is what there is.

I put this in my .idf:

    Output:Meter,*:*,HOURLY;

That should dump all HOURLY meters. "*:*" is the Name field of the Output:Meter Object. IDF itself
doesn't know how to interpret this field. It just need to be a string, but it has structure defined
in the IDD informally:

   \note Form is EnergyUseType:..., e.g. Electricity:* for all Electricity meters
   \note or EndUse:..., e.g. GeneralLights:* for all General Lights

Resulting in outputs labelled like, Refrigeration:Electricity [J](Hourly),  Electricity:Facility [J](Monthly).
The first field can be either an EndUse or a EnergyUseType, it's up to the use to figure out which
based on knowledge of the model.
'''
import sys
import numpy as np
import pandas as pd
import re
from pprint import pprint
from parse_eplus_out_csv import *


all_cols = []
all_cols_basename = []
for f in sys.argv[1:]:
  print(f)
  cols = eplus_energy_l0_p(f).columns.tolist()
  print((cols))
  all_cols += cols
  all_cols_basename += list(map(lambda n: re.sub('(^[^:]+):.*', '\\1', n), cols))
print('-'*100)
pprint(sorted(set(all_cols)))
pprint(sorted(set(all_cols_basename)))
